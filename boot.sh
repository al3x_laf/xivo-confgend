#!/bin/bash
if [ -f "/root/twistd.pid" ]; then
	rm "/root/twistd.pid"
fi

XIVO_CONFGEND_FILE="/etc/xivo/xivo-confgend.conf"

cat > $XIVO_CONFGEND_FILE << EOF
[confgend]
listen=0.0.0.0
port=8669

; cache path
cache=/var/lib/xivo-confgend

; Database connection informations.
db_uri=postgresql://asterisk:proformatique@db/asterisk

[asterisk]
contextsconf=/etc/xivo/xivo-confgend/asterisk/contexts.conf

EOF

if [ -n "${IS_MDS}" ]; then
	cat >> $XIVO_CONFGEND_FILE <<-EOF
	; MDS configuration
	[mds]
	mds_mode=yes
	EOF
fi


/usr/bin/twistd -n -o -y /usr/local/bin/xivo-confgend --logfile=/var/log/xivo-confgend.log
