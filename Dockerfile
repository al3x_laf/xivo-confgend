## Image to build from sources

FROM python:2.7.15-slim-stretch
MAINTAINER XiVO Team "dev@avencall.com"

ENV DEBIAN_FRONTEND noninteractive
ENV HOME /root

# Add dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends --auto-remove \
    git \
    python-twisted \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Install xivo-confgend
WORKDIR /usr/src/confgend

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .
RUN python setup.py install

RUN touch /var/log/xivo-confgend.log
RUN mkdir /var/lib/xivo-confgend
RUN cp -a boot.sh /var/lib/xivo-confgend/
WORKDIR /root

# Clean source dir
RUN rm -rf /usr/src/confgend

# Configure environment
ENV PYTHONPATH=/usr/local/lib/python2.7/site-packages

# Version
ARG TARGET_VERSION
LABEL version=${TARGET_VERSION}

EXPOSE 8669
CMD ["/var/lib/xivo-confgend/boot.sh"]
