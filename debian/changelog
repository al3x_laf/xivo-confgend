xivo-confgend (1:2020.08.00) freya; urgency=medium

  * Bump version to 2020.08.00

 -- Jean-Pierre Thomasset <jpthomasset@gmail.com>  Mon, 11 May 2020 14:33:00 +0000

xivo-confgend (1:2020.07.00) xivo-electra; urgency=medium

  [ Tomas Taraba ]
  * 3162 As a user with UA I should have only one presence state displayed for other users

  [ esevellec ]
  * 2524 add a condition to restrict sip peers loaded to the current mds 

 -- Tomas Taraba <ttaraba@avencall.com>  Thu, 02 Apr 2020 17:54:19 +0200

xivo-confgend (1:2020.05.00) xivo-electra; urgency=medium

  * 3181 - Generate WebRTC line for UA account

 -- Etienne Allovon <eallovon@avencall.com>  Thu, 05 Mar 2020 16:38:10 +0000

xivo-confgend (1:2020.03.00) xivo-electra; urgency=medium

  * 2530 Finish IAX removal

 -- Benoit SCHULER <bschuler@bus-ThinkPad-T480s>  Thu, 13 Feb 2020 10:10:49 +0100

xivo-confgend (1:2019.12.06) xivo-deneb; urgency=medium

  * 2954 Disabled extensions are still generated in the dialplan

 -- Vojtech Sodoma <vsodoma@avencall.com>  Mon, 11 Nov 2019 11:31:15 +0100

xivo-confgend (1:2019.12.02) xivo-deneb; urgency=medium

  * 2884 Update webrtc value via CSV Update

 -- Vojtech Sodoma <vsodoma@avencall.com>  Mon, 07 Oct 2019 15:29:20 +0200

xivo-confgend (1:2019.12.00) xivo-deneb; urgency=medium

  * Bump version to 2019.12.00

 -- Jean-Pierre Thomasset <jpthomasset@gmail.com>  Fri, 20 Sep 2019 09:00:08 +0000

xivo-confgend (1:2019.10.00) xivo-deneb; urgency=medium

  * 2684 Remove dependency on xivo-web-interface

 -- Vojtech Sodoma <vsodoma@avencall.com>  Wed, 04 Sep 2019 16:04:05 +0200

xivo-confgend (1:2019.05.02) xivo-callisto; urgency=medium

  * 2506 Generate hint only on mds where the user belongs

 -- Laurent Meiller <lmeiller@avencall.com>  Mon, 13 May 2019 10:37:27 +0200

xivo-confgend (1:2019.05.00) xivo-callisto; urgency=medium

  [Jean-Pierre Thomasset]
  * 2397 - Change intra-xds routing to include destination context and allow routing to subroutine

  [Tomas Taraba]
  * 2413 - Fix group generation on mds when peername is wrong
  * 2432 XDS - Generate new outcall in to-extern

  [Vojtech Sodoma]
  * 2467 Outcall application - be able to call number starting with +

 -- Jean-Pierre Thomasset <jpthomasset@gmail.com>  Tue, 02 Apr 2019 13:24:44 +0000

xivo-confgend (1:2019.04.00) xivo-callisto; urgency=medium

  * New Routing strategy for inter-mds communication

 -- Jean-Pierre Thomasset <jpthomasset@gmail.com>  Thu, 21 Mar 2019 10:29:27 +0000

xivo-confgend (1:2019.03.00) xivo-callisto; urgency=medium

  [ Laurent Meiller ]
  * 2347 Add find queue settings by queuefeatures or groupfeatures
  * 2348 Allow routing to local MDS group or use xds_routing as fallback

  [ Vojtech Sodoma ]
  * 2361 Generate SIP registration only for local trunks of the MDS

 -- Laurent Meiller <lmeiller@avencall.com>  Thu, 28 Feb 2019 11:41:28 +0100

xivo-confgend (1:2019.02.00) xivo-callisto; urgency=medium

  * 2353 Generate sip configuration with local trunks on a MDS

 -- Vojtech Sodoma <vsodoma@avencall.com>  Mon, 11 Feb 2019 14:48:56 +0100

xivo-confgend (1:2019.01.00) xivo-callisto; urgency=medium

  * 2242 XDS - Dial users in different context attached to a different mds fails

 -- Tomas Taraba <ttaraba@avencall.com>  Mon, 14 Jan 2019 17:05:46 +0100

xivo-confgend (1:2018.16.03) xivo-borealis; urgency=medium

  * 2219 XDS - Incoming call (DID) to a user located on a MDS (which is not mds0) doesn't work

 -- Tomas Taraba <ttaraba@avencall.com>  Tue, 04 Dec 2018 16:15:11 +0100

xivo-confgend (1:2018.16.01) xivo-borealis; urgency=medium

  * 2134 Be able to skip echotest intro

 -- Etienne Allovon <eallovon@avencall.com>  Thu, 18 Oct 2018 12:23:23 +0200

xivo-confgend (1:2018.15.00) xivo-borealis; urgency=medium

  * 2093 XDS - Be able to route calls depending on the MDS of the peer

 -- Tomas Taraba <ttaraba@avencall.com>  Thu, 27 Sep 2018 15:38:12 +0200

xivo-confgend (1:2018.13.00) xivo-borealis; urgency=medium

  * Bump version 2018.13.00

 -- Tomas Taraba <ttaraba@avencall.com>  Tue, 04 Sep 2018 09:34:38 +0200

xivo-confgend (1:2018.12.00) xivo-borealis; urgency=medium

  * Convert xivo-confgend to docker container

 -- Frederic Servian <fservian@avencall.com>  Thu, 16 Aug 2018 07:56:31 +0200

xivo-confgend (1:2018.11.00) xivo-borealis; urgency=medium

  * 2000 Add MDS mode

 -- Etienne Allovon <eallovon@avencall.com>  Tue, 31 Jul 2018 16:36:15 +0200

xivo-confgend (1:2018.04.00) xivo-aldebaran; urgency=medium

  * 1682 Accept Options requests with 200 OK

 -- Vojtech Sodoma <vsodoma@avencall.com>  Mon, 26 Feb 2018 16:44:56 +0100

xivo-confgend (1:2018.02.00) xivo-aldebaran; urgency=medium

  * 1309 Add video options to SIP peers

 -- Jirka HLAVACEK <jhlavacek@avencall.com>  Tue, 23 Jan 2018 16:07:46 +0100

xivo-confgend (1:2018.01.00) xivo-aldebaran; urgency=medium

  * 1145 add all agents to queueskills.conf
  * 1603 disable voicemail operator in voicemail.conf

 -- Vojtech Sodoma <vsodoma@avencall.com>  Wed, 29 Nov 2017 12:02:32 +0100

xivo-confgend (1:2017.11.06) xivo-dev; urgency=medium

  * default allow_wrapup_termination option to yes for queues.conf

 -- Etienne Allovon <eallovon@avencall.com>  Wed, 22 Nov 2017 12:38:11 +0100

xivo-confgend (1:2017.11.03) xivo-dev; urgency=medium

  * increase WebRTC call-limit to 2 (#1308)

 -- Jirka HLAVACEK <jhlavacek@avencall.com>  Mon, 16 Oct 2017 11:24:54 +0200

xivo-confgend (1:2017.11.00) xivo-dev; urgency=medium

  * show XiVO version in User-Agent SIP header
  * change trust_id_outbound value to yes

 -- Vojtech Sodoma <vsodoma@avencall.com>  Tue, 19 Sep 2017 08:54:15 +0200

xivo-confgend (1:2017.10.00) xivo-dev; urgency=medium

  * set rtcp_mux to yes for webrtc lines

 -- Jirka Hlavacek <jhlavacek@avencall.com>  Thu, 31 Aug 2017 16:05:54 +0200

xivo-confgend (1:2017.05.00) xivo-dev; urgency=medium

  * include login and pause agent extension (instead of whole context)

 -- Etienne Allovon <eallovon@avencall.com>  Wed, 24 May 2017 15:57:48 +0200

xivo-confgend (1:2017.03~20170315.161724) xivo-dev; urgency=low

  * set rtptimeout for webrtc lines

 -- Vojtech Sodoma <vsodoma@avencall.com>  Wed, 15 Mar 2017 16:17:24 +0100

xivo-confgend (1:2017.02~20170223.135543) xivo-dev; urgency=low

  * include login and pause agent with func keys in internal context

 -- Vojtech Sodoma <vsodoma@avencall.com>  Thu, 23 Feb 2017 13:55:43 +0100

xivo-confgend (1:2017.02~20170222.154146) xivo-dev; urgency=low

  * remove mohtest from internal context

 -- Tomas Taraba <ttaraba@avencall.com>  Wed, 22 Feb 2017 15:41:46 +0100

xivo-confgend (1:2017.02~20170217.145346) xivo-dev; urgency=low

  * include echotest and mohtest in internal context

 -- Tomas Taraba <ttaraba@avencall.com>  Fri, 17 Feb 2017 14:53:46 +0100

xivo-confgend (1:15.03~20150210.164916.a49ee01) xivo-dev; urgency=low

  * rename xivo-confgen to xivo-confgend

 -- Sebastien Duthil <sduthil@avencall.com>  Tue, 17 Feb 2015 09:24:04 -0500

xivo-confgen (1:14.01~20140113.151047.8d8996e-wheezy-5) xivo-dev; urgency=low

  * add epoch in version limiting moving conffile

 -- Sebastien Duthil <sduthil@avencall.com>  Thu, 16 Jan 2014 08:22:24 -0500

xivo-confgen (1:14.01~20140113.151047.8d8996e-wheezy-4) xivo-dev; urgency=low

  * remove the double DEBHELPER tag in xivo-confgend postinst

 -- Pascal Cadotte Michaud <pcadottemichaud@avencall.com>  Tue, 14 Jan 2014 13:22:12 -0500

xivo-confgen (1:14.01~20140113.151047.8d8996e-wheezy-3) xivo-dev; urgency=low

  * use maintscript instead of hand written postinst to rename pf-xivo to xivo

 -- Pascal Cadotte Michaud <pcadottemichaud@avencall.com>  Tue, 14 Jan 2014 09:40:58 -0500

xivo-confgen (1:14.01~20140113.151047.8d8996e-wheezy-2) xivo-dev; urgency=low

  * rename configuration paths from pf-xivo to xivo when copying conffiles

 -- Sebastien Duthil <sduthil@avencall.com>  Mon, 13 Jan 2014 11:13:16 -0500

xivo-confgen (1:13.23~20131106.152948.eae555e-2) squeeze-xivo-skaro-dev; urgency=low

  * depends on xivo-dao

 -- Sebastien Duthil <sduthil@avencall.com>  Tue, 19 Nov 2013 09:28:42 -0500

xivo-confgen (1:13.04~20130220.204524.daf172e-4) squeeze-xivo-skaro-dev; urgency=low

  * revert init script

 -- Nicolas HICHER <nhicher@avencall.com>  Sat, 23 Feb 2013 08:52:20 -0500

xivo-confgen (1:13.04~20130220.204524.daf172e-2) squeeze-xivo-skaro-dev; urgency=low

  * fix init script (status on stopped service must return 3)

 -- Nicolas HICHER <nhicher@avencall.com>  Fri, 22 Feb 2013 12:36:19 -0500

xivo-confgen (1:13.04~20130125.161225.0e59336-2) squeeze-xivo-skaro-dev; urgency=low

  * depends on xivo-web-interface

 -- Nicolas HICHER (atarakt) <nhicher@proformatique.com>  Tue, 19 Feb 2013 14:17:14 -0500

xivo-confgen (1:13.03~20130125.161225.0e59336-2) squeeze-xivo-skaro-dev; urgency=low

  * add default file for xivo

 -- Cedric Abunar <cabunar@avencall.com>  Tue, 05 Feb 2013 10:05:17 -0500

xivo-confgen (1:12.22~20121106.150019.36f1a82-2) squeeze-xivo-skaro-dev; urgency=low

  * fix depends, now on xivo-lib-python

 -- Nicolas HICHER (atarakt) <nhicher@proformatique.com>  Wed, 07 Nov 2012 11:21:34 -0500

xivo-confgen (1:1.2~beta-2-1) squeeze-xivo-skaro-dev; urgency=low

  * fix version

 -- Nicolas HICHER (atarakt) <nhicher@proformatique.com>  Mon, 12 Dec 2011 13:31:22 -0500

xivo-confgen (1:1.2~20110804.172823.296324a-3) squeeze-xivo-skaro-dev; urgency=low

  * package renamed to xivo-confgen

 -- Jean-Yves LEBLEU <jylebleu@avencall.com>  Fri, 21 Oct 2011 15:42:56 -0400

pf-xivo-confgen (1:1.2~20110804.172823.296324a-2) squeeze-xivo-skaro-dev; urgency=low

  * add status in initscript help message.

 -- Etienne Lessard <edesrocherslessard@avencall.com>  Thu, 11 Aug 2011 10:52:07 -0400

pf-xivo-confgen (1:1.2~svn10243-0) squeeze-xivo-skaro-dev; urgency=low

  * new package pf-xivo-libconfgend

 -- Nicolas HICHER (atarakt) <nhicher@proformatique.com>  Wed, 23 Feb 2011 11:02:37 +0100

pf-xivo-confgen (1:1.2~svn10232-1) squeeze-xivo-skaro-dev; urgency=low

  * [AUTOBUILD] Rebuild.
  * [AUTOBUILD] New upstream development snapshot.

 -- Proformatique Maintainance Team <technique@proformatique.com>  Fri, 18 Feb 2011 08:16:13 +0100

pf-xivo-confgen (1:1.2~svn10211-0) pf-xivo-skaro-dev; urgency=low

  * Fix for nono

 -- Sylvain Boily <sboily@proformatique.com>  Wed, 16 Feb 2011 13:07:23 -0500

pf-xivo-confgen (1:1.2~svn9859-0) pf-xivo-skaro-dev; urgency=low

  *  new setup.py

 -- Nicolas HICHER (atarakt) <nhicher@proformatique.com>  Tue, 04 Jan 2011 16:25:22 +0100

pf-xivo-confgen (1.2.0-0) pf-xivo-skaro-dev; urgency=low

  * Initial release.

 -- Nicolas HICHER (atarakt) <nhicher@proformatique.com>  Tue, 07 Dec 2010 15:39:42 +0100
