xivo-confgend
=============

xivo-confgend is a service for generating IPBX configuration files.

The service is used through the xivo-confgen client from the xivo-confgend-client project.

Commands are sent in this format:

    /usr/bin/xivo-confgen [frontend]/[conffile]

xivo-confgend generates the requested file, and:

- (re)writes it in `/var/lib/xivo-confgend/[frontend]`
- prints it to stdout


Running unit tests
------------------

```
apt-get install libpq-dev python-dev libffi-dev libyaml-dev
pip install tox
tox --recreate -e py27
```


## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

See the COPYING file for details.
